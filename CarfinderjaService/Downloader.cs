﻿using CarfinderjaWeb.Data.Repositories;
using CarfinderjaWeb.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CarfinderjaService
{
    public class Downloader : IDisposable
    {
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        private readonly ConcurrentDictionary<string, ConcurrentQueue<DownloadTask>> _downloadTasks =
            new ConcurrentDictionary<string, ConcurrentQueue<DownloadTask>>();
        private readonly ConcurrentBag<Task> _workers = new ConcurrentBag<Task>();
        private readonly int _hostQueueDelayMilliseconds;
        private readonly int _retryPeriodMilliseconds;
        private readonly int _failingAttemptsMaxCount;
        private readonly int _emptyQueueCheckingAttemptsCount = 10;

        private readonly IStatisticsService _statisticsService;
        private readonly ISiteStatisticsRepository _siteStatisticsRepository;

        public Downloader(int hostQueueDelayMilliseconds, int retryPeriodMilliseconds, int failingAttemptsMaxCount, IStatisticsService statisticsService, ISiteStatisticsRepository siteStatisticsRepository)
        {
            _hostQueueDelayMilliseconds = hostQueueDelayMilliseconds;
            _retryPeriodMilliseconds = retryPeriodMilliseconds;
            _failingAttemptsMaxCount = failingAttemptsMaxCount;
            _statisticsService = statisticsService;
            _siteStatisticsRepository = siteStatisticsRepository;
        }

        public bool Finished
        {
            get { return _workers.All(worker => worker.IsCompleted); }
        }

        public void Stop()
        {
            _cancellationTokenSource.Cancel();
        }

        public void Download(string url, Action<string> continuation)
        {
            var downloadTask = new DownloadTask(url, continuation);
            Task queueProcessingTask = null;
            var uri = new Uri(url);
            _downloadTasks.AddOrUpdate(uri.Host, key =>
            {
                queueProcessingTask = new Task(() => ProcessDownloadQueue(uri.Host), _cancellationTokenSource.Token,
                    TaskCreationOptions.LongRunning);
                var queue = new ConcurrentQueue<DownloadTask>();
                queue.Enqueue(downloadTask);
                return queue;
            }, (key, queue) =>
            {
                queue.Enqueue(downloadTask);
                return queue;
            });
            if (queueProcessingTask != null)
            {
                queueProcessingTask.Start();
                _workers.Add(queueProcessingTask);
            }
        }

        private void ProcessDownloadQueue(string key)
        {
            ConcurrentQueue<DownloadTask> queue;
            if (!_downloadTasks.TryGetValue(key, out queue)) return;
            int counter = 0;
            while (true)
            {
                DownloadTask downloadTask;
                if (queue.TryDequeue(out downloadTask))
                {
                    if (downloadTask.Status != DownloadTaskStatus.New) continue;
                    downloadTask.Status = DownloadTaskStatus.InProgress;
                    string content = null;
                    int attempts = 0;
                    while (attempts++ < _failingAttemptsMaxCount && !GetContent(downloadTask.Url, out content))
                    {
                        Thread.Sleep(TimeSpan.FromMilliseconds(_retryPeriodMilliseconds));
                    }
                    if (string.IsNullOrWhiteSpace(content)) downloadTask.Status = DownloadTaskStatus.Failed;
                    else
                    {
                        downloadTask.Status = DownloadTaskStatus.Finished;
                        Task.Run(() => downloadTask.Continuation(content));
                    }
                    counter = 0;
                }
                else
                {
                    if (counter < _emptyQueueCheckingAttemptsCount) counter++;
                    else
                    {
                        _downloadTasks.TryRemove(key, out queue);
                        break;
                    }
                }
                Thread.Sleep(TimeSpan.FromMilliseconds(_hostQueueDelayMilliseconds));
            }
        }

        private bool GetContent(string url, out string result)
        {
            result = null;
            bool succeeded = false;
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.UserAgent.ParseAdd("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36");
                try
                {
                    result = client.GetStringAsync(url).Result;
                    succeeded = true;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Download succeeded: {0}", url);
                }
                catch (AggregateException ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Downloading error: {0}", url);
                    //Console.WriteLine(ex);
                    var inner = ex.InnerExceptions.First();
                    if (!(inner is HttpRequestException || inner is TaskCanceledException)) throw;
                }
            }
            return succeeded;
        }

        public void ComputeStats()
        {
            var stats = _statisticsService.GetStatistics();
            var statsJson = JsonConvert.SerializeObject(stats);
            _siteStatisticsRepository.SaveSiteStatistics(statsJson);
        }

        public void Wait()
        {
            Task.WaitAll(_workers.ToArray());
        }

        public string GetDownloadingStats()
        {
            var builder = new StringBuilder("\nDownload stats:\n");
            foreach (var item in _downloadTasks)
            {
                builder.AppendFormat("Site: {0}. {1} downloads remaining.\n", item.Key, item.Value.Count);
            }
            builder.AppendLine();
            return builder.ToString();
        }

        public void Dispose()
        {
            _cancellationTokenSource.Dispose();
        }
    }
}