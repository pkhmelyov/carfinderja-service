﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class CarsInJamaica : Parser {
		private const string YearPropertyName = "Year:";
		private const string TransmissionPropertyName = "Transmission";
		private const string LocationPropertyName = "Location:";

	    public CarsInJamaica(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.CarsInJamaicaCom; } }
		public override string FirstPageAddress { get { return "http://carsinjamaica.com/?paged=1"; } }
		public override Url BaseUrl { get { return new Url("http://carsinjamaica.com/"); } }
		private Dictionary<string, string> _properties;
		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			string query = "#page-content .pagination a.page-numbers:not(.next)";
			var lastPageAnchor = document.QuerySelectorAll<IHtmlAnchorElement>(query).Last();
			int lastPageIndex = 1;
			int.TryParse(lastPageAnchor.Text, out lastPageIndex);
			return Enumerable.Range(1, lastPageIndex)
				.Select(page => string.Format("http://carsinjamaica.com/?paged={0}", page));
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			string query = "#page-content .grid-column .single-vehicle-view .single-desc a";
			var advertisementAncors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			return advertisementAncors.Select(anchor => anchor.Href);
		}

		protected override string GetName(IHtmlDocument document) {
			string query = "#header-left-content h1";
			var header = document.QuerySelector<IHtmlHeadingElement>(query);
			if(header!=null) return header.Text();
			return null;
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			string query = "#slider img.entry-image";
			var image = document.QuerySelector<IHtmlImageElement>(query);
			string src = image != null ? image.GetAttribute("src") : "";
			if (string.IsNullOrEmpty(src)) {
				return DefaultThumbnailUrl;
			}
			return src;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			string query = "#vehicle-overview .single-price";
			var element = document.QuerySelector(query);
			if (element == null) return null;
			string text = element.TextContent.Trim().TrimStart('$').Replace(",", "");
			decimal result;
			if (!decimal.TryParse(text, out result)) {
				return null;
			}
			return result;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			_properties = GetAllProperties(document);
			Transmission result;
			if (!Enum.TryParse(GetPropertyValue(TransmissionPropertyName, s => s.Trim()), true, out result)) {
				return null;
			}
			return result;
		}

		protected override int? GetYear(IHtmlDocument document) {
			_properties = GetAllProperties(document);
			int result;
			if (!int.TryParse(GetPropertyValue(YearPropertyName, s => s.Trim()), out result)) {
				return null;
			}
			return result;
		}

		protected override string GetContactName(IHtmlDocument document) {
			return "";
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			return "";
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return "";
		}
			
		protected override string GetLocation(IHtmlDocument document) {
			_properties = GetAllProperties(document);
			return GetPropertyValue(LocationPropertyName, s => s.Trim());
		}

		private Dictionary<string, string> GetAllProperties(IHtmlDocument document) {
			return document.QuerySelectorAll<IHtmlDivElement>("#vehicle-overview .features_table .line")
					.Select(row => new { Name = row.Children[0].TextContent.Trim(), Value = row.Children[1].TextContent.Trim() })
					.ToDictionary(x => x.Name, x => x.Value);
		}

		private string GetPropertyValue(string key, Func<string, string> normalizeValue) {
			string propertyValue;
			if (_properties.TryGetValue(key, out propertyValue) && normalizeValue != null) {
				propertyValue = normalizeValue(propertyValue);
			}
			return propertyValue;
		}
	}
}