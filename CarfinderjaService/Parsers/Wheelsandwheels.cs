﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class Wheelsandwheels : Parser {
		private const string MakePropertyName = "Make";
		private const string ModelPropertyName = "Model";
		private const string PricePropertyName = "Price";
		private const string TransmissionPropertyName = "Transmission";
		private const string YearPropertyName = "Year";
		private const string DealerPropertyName = "Dealer";

        public Wheelsandwheels(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.WheelsandwheelsCom; } }
		public override string FirstPageAddress { get { return "http://www.wheelsandwheelsauto.com/index.php?option=com_autostand&func=special&sorting=all"; } }
		public override Url BaseUrl { get { return new Url("http://www.wheelsandwheelsauto.com/"); } }
		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			return new[] {FirstPageAddress};
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			var query =
				@"td[width=""63%""] > img[src=""http://www.wheelsandwheelsauto.com/images/M_images/arrow.png""] + a";
			return document.QuerySelectorAll<IHtmlAnchorElement>(query).Select(a => new Url(BaseUrl, a.Href.Replace("about://", "")).Href);
		}

		protected override string GetName(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			if (properties == null) return null;
			var make = GetPropertyValue(properties, MakePropertyName);
			var model = GetPropertyValue(properties, ModelPropertyName);
			if (string.IsNullOrEmpty(make) && string.IsNullOrEmpty(model)) return null;
			return string.Format("{0} {1}", make, model);
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var query = @"td[width=""57%""] img:first-child";
			var img = document.QuerySelector<IHtmlImageElement>(query);
			if (img != null) {
				return img.Source;
			}
			return DefaultThumbnailUrl;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			if (properties != null) {
				var text = GetPropertyValue(properties, PricePropertyName, value => value.Replace("$", "").Replace(",", "").Trim());
				decimal result;
				if (decimal.TryParse(text, out result)) return result;
			}
			return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			if (properties != null) {
				var text = GetPropertyValue(properties, TransmissionPropertyName, value => value.ToLowerInvariant().Replace("semi automatic", "tiptronic").Split(new [] {' '}, StringSplitOptions.RemoveEmptyEntries)[0]);
				Transmission result;
				if (Enum.TryParse(text, true, out result)) return result;
			}
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			if (properties != null) {
				var text = GetPropertyValue(properties, YearPropertyName);
				int result;
				if (int.TryParse(text, out result)) return result;
			}
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			if (properties != null) {
				var text = GetPropertyValue(properties, DealerPropertyName,
					value => {
						var index = value.IndexOf("VIN#");
						return value.Substring(0, index).Trim();
					});
				return text;
			}
			return null;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			var query = "#dmail";
			var select = document.QuerySelector<IHtmlSelectElement>(query);
			if (select != null) {
				var text = select.TextContent;
				return text;
			}
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			return null;
		}

		private Dictionary<string, string> GetAllProperties(IHtmlDocument document) {
			var result = new Dictionary<string, string>();
			var query = @"td[width=""43%""] td";
			var cells = document.QuerySelectorAll<IHtmlTableDataCellElement>(query);
			if (cells != null) {
				result = cells.Where(cell => cell.TextContent.Trim().IndexOf(":") >= 0).ToDictionary(
					cell => {
						var text = cell.TextContent.Trim();
						var index = text.IndexOf(":", StringComparison.InvariantCultureIgnoreCase);
						var key = text.Substring(0, index).Trim();
						return key;
					},
					cell => {
						var text = cell.TextContent.Trim();
						var index = text.IndexOf(":", StringComparison.InvariantCultureIgnoreCase);
						var val = text.Substring(index + 1).Trim();
						return val;
					}
				);
			}
			return result;
		}

		private string GetPropertyValue(Dictionary<string, string> properties, string key,
			Func<string, string> normalize = null) {
			if (properties == null) return null;
			if (!properties.ContainsKey(key)) return null;
			if (normalize != null) return normalize(properties[key]);
			return properties[key];
		}
	}
}
