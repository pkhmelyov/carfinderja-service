﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class Superlativeauto : Parser {
		private const string MakePropertyName = "Make";
		private const string ModelPropertyName = "Model";
		private const string TransmissionPropertyName = "Transmission";
		private const string YearPropertyName = "Make Year";

	    public Superlativeauto(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.SuperlativeautoCom; } }
		public override string FirstPageAddress { get { return "http://superlativeauto.com/stock_list?&page=1"; } }
		public override Url BaseUrl { get { return new Url("http://superlativeauto.com/"); } }
		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			var query = "#page .pagination li:last-child a";
			var lastAnchor = document.QuerySelector<IHtmlAnchorElement>(query);
			if (lastAnchor != null) {
				var lastUrl = new Url(lastAnchor.Href);
				var queryString = lastUrl.Query;
				var queryParams = HttpUtility.ParseQueryString(queryString);
				int lastPageIndex;
				int.TryParse(queryParams["page"], out lastPageIndex);
				return
					Enumerable.Range(1, lastPageIndex)
						.Select(page => string.Format("http://superlativeauto.com/stock_list?&page={0}", page));
			}
			return null;
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			var query = "#page .itemlist a";
			return document.QuerySelectorAll<IHtmlAnchorElement>(query).Select(a => a.Href);
		}

		protected override string GetName(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var make = GetPropertyValue(properties, MakePropertyName, value => value.Trim());
			var model = GetPropertyValue(properties, ModelPropertyName, value => value.Trim());
			return string.Format("{0} {1}", make, model);
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var query = "#gallery img";
			var imgs = document.QuerySelectorAll<IHtmlImageElement>(query).ToArray();
			if (imgs.Length > 0) {
				var img = imgs[0];
				if (img.Source == BaseUrl.Href && imgs.Length > 1) img = imgs[1];
				return string.IsNullOrEmpty(img.Source) ? DefaultThumbnailUrl : img.Source;
			}
			return DefaultThumbnailUrl;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var transmission = GetPropertyValue(properties, TransmissionPropertyName,
				value => value.ToLowerInvariant().Replace("auto", "automatic").Trim());
			if (transmission == null) return null;
			Transmission result;
			if (Enum.TryParse(transmission, true, out result)) return result;
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var year = GetPropertyValue(properties, YearPropertyName, value => value.Trim());
			if (year == null) return null;
			int result;
			if (int.TryParse(year, out result)) return result;
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			return null;
		}

		private Dictionary<string, string> GetAllProperties(IHtmlDocument document) {
			var query = "#page table tr";
			return document.QuerySelectorAll<IHtmlTableRowElement>(query).ToDictionary(row => row.Cells[0].TextContent.Trim(), row => row.Cells[1].TextContent.Trim());
		}

		private string GetPropertyValue(Dictionary<string, string> properties, string key, Func<string, string> normalize) {
			if (properties.ContainsKey(key)) {
				if (normalize != null) return normalize(properties[key]);
				return properties[key];
			}
			return null;
		}
	}
}
