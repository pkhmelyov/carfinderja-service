﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class Jamaicanclassifieds : Parser {
		private const string TransmissionPropertyName = "Transmission";
		private const string YearPropertyName = "Year";
		private const string ContactNamePropertyName = "Posted by";
		private const string TelPropertyName = "Tel";
		private const string LocationPropertyName = "Location";
		private const string PricePropertyName = "Price";

	    public Jamaicanclassifieds(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.JamaicanclassifiedsCom; } }
		public override string FirstPageAddress { get { return "http://www.jamaicanclassifieds.com/viewlist.asp?cid=10029&sid=50779&sa=1&page=1"; } }
		public override Url BaseUrl { get { return new Url("http://www.jamaicanclassifieds.com/"); } }
		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			var query = @"td[height=""350""] > :nth-child(2) > a:last-child";
			var link = document.QuerySelector<IHtmlAnchorElement>(query);
			var url = new Url(BaseUrl, link.Href);
			var queryString = url.Query;
			var parsedQueryString = HttpUtility.ParseQueryString(queryString);
			var lastPageNumber = int.Parse(parsedQueryString["page"]);
			return
				Enumerable.Range(1, lastPageNumber)
					.Select(
						page => string.Format("http://www.jamaicanclassifieds.com/viewlist.asp?cid=10029&sid=50779&sa=1&page={0}", page));
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			var query = "table.list_table tr > td:nth-child(2) > a";
			return document.QuerySelectorAll<IHtmlAnchorElement>(query).Select(a => new Url(BaseUrl, a.Href.Replace("about://", "")).Href);
		}

		protected override string GetName(IHtmlDocument document) {
			return InternalGetName(document);
		}

		private string InternalGetName(IHtmlDocument document, bool removeYear = true) {
			var query = "table.ad_table th:first-child font";
			var font = document.QuerySelector(query);
			if (font != null) {
				var text = font.TextContent.Trim();
				if (removeYear) {
					var parts = text.Split(new[] {' ', '\n', '\t'}, StringSplitOptions.RemoveEmptyEntries);
					int year;
					if (int.TryParse(parts[0], out year)) return string.Join(" ", parts.Skip(1));
				}
				return text;
			}
			return null;
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var query = "table.ad_table tr:nth-child(3) > td:nth-child(2) img";
			var img = document.QuerySelector<IHtmlImageElement>(query);
			if (img != null && !img.Source.Contains("nopic.gif")) return new Url(BaseUrl, img.Source.Replace("about://", "")).Href;
			return DefaultThumbnailUrl;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var text = GetPropertyValue(properties, PricePropertyName,
				value => value
					.ToLowerInvariant()
					.Replace("$", "")
					.Replace(",", "")
					.Replace(".", ",")
					.Replace("(ono)", "")
					.Replace("(neg)", "")
					.Replace("(neg.)", "")
					.Replace("neg", "")
					.Replace("negotiable", "")
					.Trim());
			if (text != null) {
				bool million = false;
				if (text.Contains("million")) { text = text.Replace("million", "").Trim(); million = true; }

				decimal price;
				if (decimal.TryParse(text, out price)) {
					if (million) price *= 1000000;
					return price;
				}
			}

			var query = "#descr > div";
			var divs = document.QuerySelectorAll<IHtmlDivElement>(query);
			foreach (var div in divs) {
				var price = ExtractPrice(div.TextContent);
				if (price.HasValue) return price;
			}
			return null;
		}

		public decimal? ExtractPrice(string text) {
			text = text.ToLowerInvariant();
			int priceIndex = text.IndexOf("price");
			if (priceIndex < 0) return null;
			text = text.Substring(priceIndex).Trim();
			text = text
					.Replace("price", "")
					.Replace(":", "")
					.Replace("$", "")
					.Replace(",", "")
					.Replace(".", ",")
					.Replace("(ono)", "")
					.Replace("(neg)", "")
					.Replace("(neg.)", "")
					.Replace("negotiable", "")
					.Trim();
			var index = text.IndexOf("tel");
			if (index >= 0) text = text.Substring(0, index).Trim();
			bool million = false;
			if (text.Contains("million")) { text = text.Replace("million", "").Trim(); million = true; }
			decimal price;
			if (decimal.TryParse(text, out price)) {
				if (million) price *= 1000000;
				return price;
			}
			return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var value = GetPropertyValue(properties, TransmissionPropertyName);
			if (value != null) {
				value = value.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).First();
				Transmission result;
				if (Enum.TryParse(value, true, out result)) return result;
			}
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var value = GetPropertyValue(properties, YearPropertyName);
			if (value != null) {
				int result;
				if (int.TryParse(value, out result)) return result;
			}
			var nameWithYear = InternalGetName(document, removeYear: false);
			if (!string.IsNullOrEmpty(nameWithYear)) {
				var index = nameWithYear.IndexOf("(", StringComparison.InvariantCultureIgnoreCase);
				if (index > 0) nameWithYear = nameWithYear.Substring(0, index);
				int year;
				var parts = nameWithYear.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
				if (int.TryParse(parts.First(), out year) || int.TryParse(parts.Last(), out year)) return year;
			}
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			return GetPropertyValue(properties, ContactNamePropertyName);
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			return GetPropertyValue(properties, TelPropertyName);
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			return GetPropertyValue(properties, LocationPropertyName);
		}

		private Dictionary<string, string> GetAllProperties(IHtmlDocument document) {
			var query = "td.ad_table_light_cell span";
			var spans = document.QuerySelectorAll<IHtmlSpanElement>(query).Skip(2).ToArray();
			var result = new Dictionary<string, string>();
			for (int i = 0; i < spans.Length; i += 2) {
				var key = spans[i].TextContent.Trim(':').Trim();
				var value = spans[i + 1].TextContent.Trim();
				result[key] = value;
			}
			return result;
		}

		private string GetPropertyValue(Dictionary<string, string> properties, string key, Func<string, string> normalize = null) {
			if (!properties.ContainsKey(key)) return null;
			if (normalize != null) return normalize(properties[key]);
			return properties[key];
		}
	}
}