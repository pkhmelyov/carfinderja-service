﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using CarfinderjaWeb.Data;
using Autofac;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers
{
    public class Crystalvehiclesales : Parser
    {
        public Crystalvehiclesales(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

        public override WebSite WebSite { get { return WebSite.CrystalvehiclesalesCom; } }
        public override string FirstPageAddress { get { return "http://www.crystalvehiclesales.com/content/all-inventory?page=0"; } }
        public override Url BaseUrl { get { return new Url("http://www.crystalvehiclesales.com/"); } }

        protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document)
        {
            string query = "ul.pager li.pager-last a";
            var anchor = document.QuerySelector<IHtmlAnchorElement>(query);
            var href = anchor.Href;
            var uri = new Uri(href);
            var queryString = uri.Query;
            var queryParams = HttpUtility.ParseQueryString(queryString);
            int lastPageIndex;
            int.TryParse(queryParams["page"], out lastPageIndex);
            return Enumerable.Range(0, lastPageIndex)
                .Select(page => string.Format("http://www.crystalvehiclesales.com/content/all-inventory?page={0}", page));
        }

        protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document)
        {
            string query = "#node-46 table tbody tr td:nth-child(1) a";
            var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
            return anchors.Select(x => new Url(BaseUrl, x.PathName).Href);
        }

        protected override string GetName(IHtmlDocument document)
        {
            string makeQuery = ".field-field-make .field-item";
            var makeElement = document.QuerySelector(makeQuery);
            string make = "";
            if (makeElement != null)
            {
                make = makeElement.TextContent.Replace("Make:", "").Trim();
            }
            string modelQuery = ".field-field-model .field-item";
            var modelElement = document.QuerySelector(modelQuery);
            string model = "";
            if (modelElement != null)
            {
                model = modelElement.TextContent.Replace("Model:", "").Trim();
            }
            return string.Format("{0} {1}", make, model);
        }

        protected override string GetThumbnailUrl(IHtmlDocument document)
        {
            var imageElement = document.QuerySelector("#content-content div.content div.field-type-filefield img");
            string src = imageElement != null ? imageElement.GetAttribute("src") : "";
            if (string.IsNullOrEmpty(src))
            {
                return DefaultThumbnailUrl;
            }
            return src;
        }

        protected override decimal? GetPrice(IHtmlDocument document)
        {
            var query = "#content-content div.content div.field-field-price p";
            var element = document.QuerySelector<IHtmlParagraphElement>(query);
            if (element != null)
            {
                var parts = element.TextContent.ToLowerInvariant().Replace("on sale", "").Replace("sale", "").Replace("$", "").Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                var text = parts.Length > 0 ? parts[0].Replace(".00", "").Replace(".", "").Replace(",", "") : "";
                decimal result;
                if (decimal.TryParse(text, out result)) return result;
            }
            return null;
        }

        protected override Transmission? GetTransmission(IHtmlDocument document)
        {
            var query = "#content-content div.content div.field-field-trantype p";
            var element = document.QuerySelector<IHtmlParagraphElement>(query);
            if (element != null)
            {
                var text = element.TextContent.Trim();
                Transmission result;
                if (Enum.TryParse(text, true, out result))
                {
                    return result;
                }
            }
            return null;
        }

        protected override int? GetYear(IHtmlDocument document)
        {
            var query = "#content-content div.content div.field-field-year .field-item";
            var element = document.QuerySelector<IHtmlDivElement>(query);
            if (element != null)
            {
                var text = element.TextContent.Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Last();
                int result;
                if (int.TryParse(text, out result)) return result;
            }
            return null;
        }

        protected override string GetContactName(IHtmlDocument document)
        {
            return null;
        }

        protected override string GetContactPhone(IHtmlDocument document)
        {
            return null;
        }

        protected override string GetContactEmail(IHtmlDocument document)
        {
            return null;
        }

        protected override string GetLocation(IHtmlDocument document)
        {
            return null;
        }
    }
}
