﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class Jamaicaobserver : Parser {
		private const string PricePropertyName = "Price";
		private const string YearPropertyName = "Year";

	    public Jamaicaobserver(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.JamaicaobserverCom; } }
		public override string FirstPageAddress { get { return "http://classifieds.jamaicaobserver.com/kingston/motor-vehicles-for-sale/search?limit=50&p=1"; } }
		public override Url BaseUrl { get { return new Url("http://classifieds.jamaicaobserver.com/"); } }
		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			var query = "#ap_summary_paginator a";
			var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			if (anchors != null) {
				var lastPageAnchor = anchors.Reverse().Skip(1).FirstOrDefault();
				if (lastPageAnchor != null) {
					var queryString = new Url(BaseUrl, lastPageAnchor.Href).Query;
					var lastPageNumber = int.Parse(HttpUtility.ParseQueryString(queryString)["p"]);
					return
						Enumerable.Range(1, lastPageNumber)
							.Select(
								p =>
									string.Format("http://classifieds.jamaicaobserver.com/kingston/motor-vehicles-for-sale/search?limit=50&p={0}",
										p));
				}
			}
			return new [] { FirstPageAddress };
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			var query = "#ap_ads #ap_title a";
			var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			if (anchors != null) {
				return anchors.Select(anchor => new Url(BaseUrl, anchor.Href.Replace("about://", "")).Href);
			}
			return null;
		}

		protected override string GetName(IHtmlDocument document) {
			var query = "#ap_detail_title";
			var div = document.QuerySelector<IHtmlDivElement>(query);
			if (div != null) {
				var parts = div.TextContent.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).ToArray();
				return string.Join(" ", parts.Take(parts.Length - 1));
			}
			return null;
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var query = "#ap_thumbphoto_1";
			var img = document.QuerySelector<IHtmlImageElement>(query);
			if (img != null) {
				return img.Source;
			}
			return DefaultThumbnailUrl;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var text = GetPropertyValue(properties, PricePropertyName, value => value.Replace("$", "").Replace(",", "").Trim());
			if (text != null) {
				decimal price;
				if (decimal.TryParse(text, out price)) return price;
			}
			return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var text = GetPropertyValue(properties, YearPropertyName);
			if (text != null) {
				int year;
				if (int.TryParse(text, out year)) return year;
			}
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			var query = "#ap_detail_phone";
			var div = document.QuerySelector<IHtmlDivElement>(query);
			if (div != null) {
				return div.TextContent.Replace("Phone:", "").Trim();
			}
			return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			return null;
		}

		private Dictionary<string, string> GetAllProperties(IHtmlDocument document) {
			var query = ".autodatafield > div";
			var divs = document.QuerySelectorAll<IHtmlDivElement>(query);
			return
				divs.Select(
					div => div.TextContent.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).ToArray())
					.ToDictionary(x => x.First(), x => x.Last());
		}

		private string GetPropertyValue(Dictionary<string, string> properties, string key,
			Func<string, string> normalize = null) {
			if (!properties.ContainsKey(key)) return null;
			if (normalize != null) return normalize(properties[key]);
			return properties[key];
		}
	}
}