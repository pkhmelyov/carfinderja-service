﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class Jacars : Parser {
		private const string PricePropertyName = "Price";
		private const string TransmissionPropertyName = "Transmission";
		private const string YearPropertyName = "Year";

		private const string PagingContainerId = "thePagingContainer";
		private const string AdvertisementLinksContainerId = "brinkcontainer";

	    public Jacars(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		private Dictionary<string, string> _properties;

		public override WebSite WebSite { get { return WebSite.JacarsNet; } }
		public override string FirstPageAddress { get { return "http://www.jacars.net/?page=browse&p=1"; } }
		public override Url BaseUrl { get { return new Url("http://www.jacars.net/"); } }

		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			string query = string.Format("#{0} div:nth-child(2) a", PagingContainerId);
			var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			return anchors.Select(x => {
				var url = new Url(x.Href);
				var builder = new UriBuilder {
					Scheme = FirstPageUrl.Scheme,
					Host = FirstPageUrl.Host,
					Path = FirstPageUrl.Path,
					Query = url.Query
				};
				return builder.Uri.ToString();
			});
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			string query = string.Format("#{0} .thumbnails-ro .hiddenInfo>a:first-child", AdvertisementLinksContainerId);
			var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			return anchors.Select(x => {
				var url = new Url(x.Href);
				var builder = new UriBuilder {
					Scheme = FirstPageUrl.Scheme,
					Host = FirstPageUrl.Host,
					Path = FirstPageUrl.Path,
					Query = url.Query
				};
				return builder.Uri.ToString();
			});
		}

		protected override string GetName(IHtmlDocument document) {
			return document.QuerySelector("#pagecontent [itemprop=name]").GetAttribute("content");
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var imageElement = document.QuerySelector("#theImages img");
			string src = imageElement != null ? imageElement.GetAttribute("src") : "";
			if (string.IsNullOrEmpty(src)) {
				return DefaultThumbnailUrl;
			}
			return new Url(BaseUrl, src).Href;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			_properties = GetAllProperties(document);
			decimal result;
			if (!decimal.TryParse(GetPropertyValue(PricePropertyName, s => s.Trim().TrimStart('$').Replace(",", "")), out result)) {
				return null;
			}
			return result;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			_properties = GetAllProperties(document);
			Transmission result;
			if (!Enum.TryParse(GetPropertyValue(TransmissionPropertyName, s => s.Trim()), true, out result)) {
				return null;
			}
			return result;
		}

		protected override int? GetYear(IHtmlDocument document) {
			_properties = GetAllProperties(document);
			int result;
			if (!int.TryParse(GetPropertyValue(YearPropertyName, s => s.Trim()), out result)) {
				return null;
			}
			return result;
		}

		protected override string GetContactName(IHtmlDocument document) {
			var query = "#ImageNinfo + div div.description-text div.description-text b";
			var element = document.QuerySelector(query);
			if (element != null) {
				return element.TextContent.Trim();
			}
			return null;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			var query = "#ImageNinfo + div div.description-text div.description-text";
			var element = document.QuerySelector(query);
			if (element != null) {
				var text = element.TextContent;
				var start = text.IndexOf("Tel:", StringComparison.InvariantCulture) + 4; // 4 = length of "Tel:"
				var end = text.IndexOf("Email:", StringComparison.InvariantCulture);
				return text.Substring(start, end - start).Trim();
			}
			return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			var query = "#ImageNinfo + div div.description-text div.description-text";
			var element = document.QuerySelector(query);
			if (element != null) {
				var text = element.TextContent;
				var start = text.IndexOf("Email:", StringComparison.InvariantCulture) + 6; // 6 = length of "Email:"
				var end = text.IndexOf("Located:", StringComparison.InvariantCulture);
				return text.Substring(start, end - start).Trim();
			}
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			var query = "#ImageNinfo + div div.description-text div.description-text";
			var element = document.QuerySelector(query);
			if (element != null) {
				var text = element.TextContent;
				var start = text.IndexOf("Located:", StringComparison.InvariantCulture) + 8; // 8 = length of "Located:"
				var end = text.IndexOf("Click the link below to see other vehicles by this seller", StringComparison.InvariantCulture);
				return text.Substring(start, end - start).Trim();
			}
			return null;
		}

		private Dictionary<string, string> GetAllProperties(IHtmlDocument document) {
			return document.QuerySelectorAll<IHtmlTableRowElement>("#ImageNinfo table.InfoList tr")
					.Select(tr => new { Name = tr.Cells[0].TextContent.Trim(), Value = tr.Cells[1].TextContent.Trim() })
					.ToDictionary(x => x.Name, x => x.Value);
		}

		private string GetPropertyValue(string key, Func<string, string> normalizeValue) {
			string propertyValue;
			if (_properties.TryGetValue(key, out propertyValue) && normalizeValue != null) {
				propertyValue = normalizeValue(propertyValue);
			}
			return propertyValue;
		}

		public override Dictionary<string, List<string>> GetMakes(IHtmlDocument document) {
			string query = "#MakeNModel .menubar li[style] a";
			var items = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			var makes = items.Skip(1).Select(x => {
				var url = new Url(x.Href);
				var queryParams = HttpUtility.ParseQueryString(url.Query);
				var make = queryParams["Brand"];
				var model = queryParams["Model"];
				return new {make, model};
			}).GroupBy(x => x.make, x => x.model).ToDictionary(x => x.Key, x => x.ToList());
			return makes;
		}
	}
}