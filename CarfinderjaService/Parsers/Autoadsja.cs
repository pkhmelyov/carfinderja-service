﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp;
using AngleSharp.Extensions;
using AngleSharp.Dom.Html;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers
{
    public class Autoadsja : Parser
    {
        public Autoadsja(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

        public override WebSite WebSite { get { return WebSite.AutoadsjaCom; } }
        public override string FirstPageAddress { get { return "http://www.autoadsja.com/search.asp?SearchSB=5&page=1"; } }
        public override Url BaseUrl { get { return new Url("http://www.autoadsja.com/"); } }

        protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document)
        {
            string query = "body > center > div > table:nth-child(2) > tbody > tr:nth-child(1) > td > center > table > tbody > tr:nth-child(9) > td:nth-child(2)";
            var cell = document.QuerySelector<IHtmlTableDataCellElement>(query);
            var totalCount = int.Parse(cell.TextContent.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0]);
            int pagesCount = totalCount / 10;
            return Enumerable.Range(1, pagesCount - 1)
                .Select(page => string.Format("http://www.autoadsja.com/search.asp?SearchSB=5&page={0}", page));
        }

        protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document)
        {
            if (document == null) return Enumerable.Empty<string>();
            string query = "body > center > div > table:nth-child(2) tr:nth-child(1) > td > center > table tr:nth-child(3) > td a, body > center > div > table:nth-child(2) tr:nth-child(1) > td > center > table tr:nth-child(7) > td a";
            var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
            return anchors.Select(x => x.Href);
        }

        protected override string GetName(IHtmlDocument document)
        {
            string query = "h1.vehicletitle";
            var title = document.QuerySelector<IHtmlHeadingElement>(query).TextContent;
            var titleParts = title.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int year;
            if (int.TryParse(titleParts[0], out year))
            {
                return string.Join(" ", titleParts.Skip(1).ToArray());
            }
            return string.Join(" ", titleParts);
        }

        protected override string GetThumbnailUrl(IHtmlDocument document)
        {
            var imageElement = document.QuerySelector("#mainpic");
            string src = imageElement != null ? imageElement.GetAttribute("src") : "";
            if (string.IsNullOrEmpty(src))
            {
                return DefaultThumbnailUrl;
            }
            return new Url(BaseUrl, src).Href;
        }

        protected override decimal? GetPrice(IHtmlDocument document)
        {
            var query = "#tab1 > table tr:nth-child(2) td b";
            var element = document.QuerySelector(query);
            if (element != null)
            {
                var text = element.TextContent.Remove(0, 8).Trim().TrimStart('$').Replace(",", "");
                decimal result;
                if (decimal.TryParse(text, out result)) return result;
            }
            return null;
        }

        protected override Transmission? GetTransmission(IHtmlDocument document)
        {
            string[] transmission =
                (document.QuerySelector("#tab1 > table tr:nth-child(8) > td:nth-child(1) > table tr:nth-child(4) > td:nth-child(2)") ?? document.QuerySelector("#tab1 > table tr:nth-child(7) > td:nth-child(1) > table tr:nth-child(4) > td:nth-child(2)") ?? document.QuerySelector("#tab1 > table tr:nth-child(3) > td:nth-child(1) > table tr:nth-child(4) > td:nth-child(2)"))
                    .TextContent.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var text = transmission[transmission.Length - 1].Trim();
            Transmission result;
            if (!Enum.TryParse(text, true, out result))
            {
                return null;
            }
            return result;
        }

        protected override int? GetYear(IHtmlDocument document)
        {
            string query = "h1.vehicletitle";
            var title = document.QuerySelector<IHtmlHeadingElement>(query).TextContent;
            var titleParts = title.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int year;
            if (int.TryParse(titleParts[0], out year))
            {
                return year;
            }
            return null;
        }

        protected override string GetContactName(IHtmlDocument document)
        {
            var query = "#tab1 > table tr:nth-child(3) > td b";
            var element = document.QuerySelector(query);
            if (element != null)
            {
                return element.TextContent.Replace("Contact name :", "").Trim();
            }
            return null;
        }

        protected override string GetContactPhone(IHtmlDocument document)
        {
            var query = "#tab1 > table tr:nth-child(4) > td b";
            var element = document.QuerySelector(query);
            if (element != null)
            {
                return element.TextContent.Replace("Contact phone :", "").Trim();
            }
            return null;
        }

        protected override string GetContactEmail(IHtmlDocument document)
        {
            return null;
        }

        protected override string GetLocation(IHtmlDocument document)
        {
            var element = document.QuerySelector("#tab1 > table tr:nth-child(8) > td > table tr:nth-child(1) > td:nth-child(2)") ?? document.QuerySelector("#tab1 > table tr:nth-child(7) > td > table tr:nth-child(1) > td:nth-child(2)") ?? document.QuerySelector("#tab1 > table tr:nth-child(3) > td > table tr:nth-child(1) > td:nth-child(2)");
            if (element != null)
            {
                return element.TextContent.Trim();
            }
            return null;
        }
    }
}
