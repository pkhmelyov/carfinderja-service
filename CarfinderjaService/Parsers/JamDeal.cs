﻿using System.Collections.Generic;
using System.Linq;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class JamDeal : Parser {
	    public JamDeal(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.JamDealCom; } }
		public override string FirstPageAddress { get { return "http://www.jamdeal.com/category/cars/264/?page=1"; } }
		public override Url BaseUrl { get { return new Url("http://www.jamdeal.com/"); } }
		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			string query = "ul.pagination";
			var pagingUl = document.QuerySelectorAll<IHtmlUnorderedListElement>(query).LastOrDefault();
			if (pagingUl != null) {
				return pagingUl.Children.Select(li => ((IHtmlAnchorElement) li.Children.First()).Href);
			}
			return Enumerable.Empty<string>();
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			string query = ".listings .list .description .title a";
			var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			if (anchors != null) {
				return anchors.Select(a => a.Href);
			}
			return Enumerable.Empty<string>();
		}

		protected override string GetName(IHtmlDocument document) {
			var query = ".title";
			var header = document.QuerySelector<IHtmlHeadingElement>(query);
			if (header != null) return header.TextContent;
			return null;
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var query = "#wrap img.img-thumbnail";
			var img = document.QuerySelector<IHtmlImageElement>(query);
			string src = img != null ? img.GetAttribute("src") : "";
			if (string.IsNullOrEmpty(src)) {
				return DefaultThumbnailUrl;
			}
			return new Url(BaseUrl, src).Href;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			var query = ".purchase .desc";
			var element = document.QuerySelector(query);
			if (element != null) {
				var strPrice = element.TextContent.Replace("Buy Now", "").Replace("JA$", "").Replace(",", "").Trim();
				decimal price;
				if (decimal.TryParse(strPrice, out price)) return price;
			}
			return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			var query = "#pane2 dd";
			var element = document.QuerySelector(query);
			if (element != null) {
				return element.TextContent.Trim();
			}
			return null;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			return null;
		}
	}
}
