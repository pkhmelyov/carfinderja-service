﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class Jetconcars : Parser {
	    public Jetconcars(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.JetconcarsCom; } }
		public override string FirstPageAddress { get { return "http://www.jetconcars.com/car-for-sale/?pages=1"; } }
		public override Url BaseUrl { get { return new Url("http://www.jetconcars.com/"); } }

		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			string query = "#main .grid_main > ul li a";
			var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			return anchors.Select(x => x.Href);
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			string query = "#main .grid_main > div > div:nth-child(1) > a";
			var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query).Skip(2);
			return anchors.Select(x => x.Href);
		}

		protected override string GetName(IHtmlDocument document) {
			string query = ".car_detail_div h3.car_title";
			var heading = document.QuerySelector<IHtmlHeadingElement>(query);
			if (heading != null) {
				var title = heading.TextContent.Trim();
				var parts = title.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
				if (parts.Length > 1) {
					int year;
					if (int.TryParse(parts[0], out year)) return string.Join(" ", parts.Skip(1));
				}
				return title;
			}
			return null;
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			string query = "#main_thumb img";
			var imageElement = document.QuerySelector(query);
			string src = imageElement != null ? imageElement.GetAttribute("src") : "";
			if (string.IsNullOrEmpty(src)) {
				return DefaultThumbnailUrl;
			}
			return src;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			var query = ".car_title_div ul li:nth-child(8)";
			var li = document.QuerySelector<IHtmlListItemElement>(query);
			if (li != null) {
				var text = li.TextContent.Replace("Price:", "").Replace("$", "").Replace(",", "").Trim();
				decimal result;
				if (decimal.TryParse(text, out result)) return result;
			}
			return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			var query = ".car_title_div ul li:nth-child(6)";
			var li = document.QuerySelector<IHtmlListItemElement>(query);
			if (li != null) {
				var text = li.TextContent.Replace("Transmission:", "");
				if(text.IndexOf("Automatic", StringComparison.InvariantCultureIgnoreCase) < 0)
					text = text.Replace("Auto", "Automatic").Trim();
				Transmission result;
				if (Enum.TryParse(text, true, out result)) return result;
			}
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			var query = ".car_detail_div h3.car_title";
			var h = document.QuerySelector<IHtmlHeadingElement>(query);
			if (h != null) {
				var parts = h.TextContent.Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				if (parts.Length > 0) {
					int result;
					if (int.TryParse(parts[0], out result)) return result;
				}
			}
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			return null;
		}
	}
}
