﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class JamaicaAutoTrader : Parser {
		private const string NameKey = "Name";
		private const string PriceKey = "Price";
		private const string TransmissionKey = "Transmission";
		private const string LocationKey = "Vehicle Location";

	    public JamaicaAutoTrader(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.JamaicaAutoTraderCom; } }
		public override string FirstPageAddress { get { return "http://jamaicaautotrader.com/search.php?Start=0"; } }
		public override Url BaseUrl { get { return new Url("http://jamaicaautotrader.com/"); } }
		private Dictionary<string, string> _properties;
		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			string query = "a.BlackLink";
			var lastPageLink = document.QuerySelectorAll<IHtmlAnchorElement>(query).LastOrDefault();
			if (lastPageLink != null) {
				var url = new Url(lastPageLink.Href);
				var queryParams = HttpUtility.ParseQueryString(url.Query);
				var start = queryParams["Start"];
				int intStart;
				if (int.TryParse(start, out intStart)) {
					int pageSize = 7;
					return Enumerable.Range(0, intStart/pageSize)
						.Select(page => string.Format("http://jamaicaautotrader.com/search.php?Start={0}", page*pageSize));
				}
			}
			return Enumerable.Empty<string>();
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			string query = "tr[onmouseover][onmouseout][onclick]";
			var rows = document.QuerySelectorAll<IHtmlTableRowElement>(query);
			return rows.Select(tr => {
				var path = tr.GetAttribute("onclick").Replace("window.open('", "").Replace("', '_top')", "");
				return new Url(BaseUrl, path).Href;
			});
		}

		protected override string GetName(IHtmlDocument document) {
			_properties = GetAllProperties(document);
			return GetPropertyValue(NameKey, null);
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var query =
				"#Table_01+table>tbody>tr:nth-child(1)>td:nth-child(2)>table>tbody>tr>td:nth-child(1)>table>tbody>tr:nth-child(3)>td img:nth-child(1)";
			var img = document.QuerySelector<IHtmlImageElement>(query);
			string src = img != null ? img.GetAttribute("src") : "";
			if (string.IsNullOrEmpty(src)) {
				return DefaultThumbnailUrl;
			}
			return new Url(BaseUrl, src).Href;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			_properties = GetAllProperties(document);
			decimal result;
			if (!decimal.TryParse(GetPropertyValue(PriceKey, s => s.Trim().TrimStart('$').Replace(",", "")), out result)) {
				return null;
			}
			return result;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			_properties = GetAllProperties(document);
			Transmission result;
			if (!Enum.TryParse(GetPropertyValue(TransmissionKey, s => s.Trim()), true, out result)) {
				return null;
			}
			return result;
		}

		protected override int? GetYear(IHtmlDocument document) {
			var name = GetName(document);
			var strYear = name.Substring(0, 4);
			int year;
			if (!int.TryParse(strYear, out year)) return null;
			return year;
		}

		protected override string GetContactName(IHtmlDocument document) {
			var query =
				"#Table_01+table>tbody>tr:nth-child(1)>td:nth-child(2)>table>tbody>tr>td:nth-child(1)>table>tbody>tr:nth-child(4)>td table:nth-child(1)+b";
			var b = document.QuerySelector(query);
			var text = b != null ? b.TextContent.Replace("For More Information Call:-", "").Trim() : null;
			return text;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			_properties = GetAllProperties(document);
			return GetPropertyValue(LocationKey, null);
		}

		private Dictionary<string, string> GetAllProperties(IHtmlDocument document) {
			var rows =
				document.QuerySelectorAll<IHtmlTableRowElement>(
					"#Table_01+table>tbody>tr:nth-child(1)>td:nth-child(2)>table>tbody>tr>td:nth-child(1)>table>tbody>tr:nth-child(4)>td table:nth-child(1) tr").ToArray();
			if(!rows.Any()) return new Dictionary<string, string>();
			var result = new Dictionary<string, string> {
				{NameKey, rows.First().Cells.First().QuerySelector("b").TextContent},
				{PriceKey, rows.First().Cells[1].TextContent.Replace(PriceKey, "").Trim(':', ' ')}
			};
			var cells = rows.Skip(1).SelectMany(tr => tr.Cells.AsEnumerable()).Where(td => !string.IsNullOrEmpty(td.TextContent.Trim()));
			foreach (var td in cells) {
				var b = td.QuerySelector("b");
				string key = b != null ? b.TextContent.Trim(':', ' ') : null;
				if(string.IsNullOrEmpty(key)) continue;
				string value = td.TextContent.Replace(key, "").Trim(':', ' ');
				result.Add(key, value);
			}
			return result;
		}
		private string GetPropertyValue(string key, Func<string, string> normalizeValue) {
			string propertyValue;
			if (_properties.TryGetValue(key, out propertyValue) && normalizeValue != null) {
				propertyValue = normalizeValue(propertyValue);
			}
			return propertyValue;
		}
	}
}
