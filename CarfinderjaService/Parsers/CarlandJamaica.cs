﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class CarlandJamaica : Parser {
	    public CarlandJamaica(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.CarlandJamaicaCom; } }
		public override string FirstPageAddress { get { return "http://carland-jamaica.com/stocklist/"; } }
		public override Url BaseUrl { get { return new Url("http://carland-jamaica.com/"); } }

		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			return new[] {FirstPageAddress};
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			string query = "#right-main > table.car-list > tbody > tr > td:nth-child(2) > a";
			var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			return anchors.Select(x => new Url(BaseUrl, x.PathName).Href);
		}

		protected override string GetName(IHtmlDocument document) {
			string query = "#title";
			var title = document.QuerySelector<IHtmlHeadingElement>(query).TextContent;
			var titleParts = title.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
			int year;
			if (int.TryParse(titleParts[0], out year)) {
				return string.Join(" ", titleParts.Skip(1).ToArray());
			}
			return string.Join(" ", titleParts);
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var imageElement = document.QuerySelector("#listing-images-wrap img:nth-child(1)");
			string src = imageElement != null ? imageElement.GetAttribute("src") : "";
			if (string.IsNullOrEmpty(src)) {
				return DefaultThumbnailUrl;
			}
			return src;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			var query = "#listing-info-wrap span.il-2";
			var span = document.QuerySelector<IHtmlSpanElement>(query);
			if (span != null) {
				var text = span.TextContent.ToLowerInvariant().Replace("semi-auto", "tiptronic").Trim();
				Transmission result;
				if (Enum.TryParse(text, true, out result)) return result;
			}
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			var query = "#title";
			var p = document.QuerySelector<IHtmlHeadingElement>(query);
			if (p != null) {
				var parts = p.TextContent.Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				var text = parts.Length > 0 ? parts[0] : "";
				int result;
				if (int.TryParse(text, out result)) return result;
			}
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			return null;
		}
	}
}
