﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class Claja : Parser {
		private const string PricePropertyName = "Price";
		private const string TransmissionPropertyName = "Tranny";
		private const string YearPropertyName = "Year";

	    public Claja(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		private Dictionary<string, string> _properties = null;

		public override WebSite WebSite { get { return WebSite.ClajaCom; } }
		public override string FirstPageAddress { get { return "http://jamaica.claja.com/0/posts/8-Vehicles/143-Cars-for-Sale/page1.html"; } }
		public override Url BaseUrl { get { return new Url("http://jamaica.claja.com/"); } }

		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			string query = ".pagelink_last";
			var lastPageAnchor = document.QuerySelector<IHtmlAnchorElement>(query);
			var lastPageHref = lastPageAnchor.Href;
			var parts = lastPageHref.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries);
			var lastPageNumber = int.Parse(parts[parts.Length - 1].Replace("page", "").Replace(".html", ""));
			return
				Enumerable.Range(1, lastPageNumber)
					.Select(i => string.Format("http://jamaica.claja.com/0/posts/8-Vehicles/143-Cars-for-Sale/page{0}.html", i));
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			var query = "#content table.postlisting tr.ad1 td a:nth-child(1), #content table.postlisting tr.ad2 td a:nth-child(1)";
			var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			return anchors.Select(x => new Url(BaseUrl, x.Href).Href);
		}

		protected override string GetName(IHtmlDocument document) {
			var query = "#content .postheader .posttitle h1";
			var h1 = document.QuerySelector<IHtmlHeadingElement>(query);
			return h1.TextContent.Trim();
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var query = "#adgallery img:nth-child(1)";
			var imageElement = document.QuerySelector(query);
			string src = imageElement != null ? imageElement.GetAttribute("src") : "";
			if (string.IsNullOrEmpty(src)) {
				return DefaultThumbnailUrl;
			}
			return new Url(BaseUrl, src).Href;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			_properties = GetProperties(document);
			var text = GetPropertyValue(_properties, PricePropertyName, value => value.Replace("$", "").Replace(",", "").Trim());
			if (text != null) {
				decimal price;
				if (decimal.TryParse(text, out price)) return price;
			}
			return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			_properties = GetProperties(document);
			var text = GetPropertyValue(_properties, TransmissionPropertyName, value => value.ToLowerInvariant().Replace("standard", "manual").Replace("manumatic", "tiptronic").Trim());
			if (text != null) {
				Transmission result;
				if (Enum.TryParse(text, true, out result)) return result;
			}
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			_properties = GetProperties(document);
			var text = GetPropertyValue(_properties, YearPropertyName, value => value.Trim());
			if (text != null) {
				int year;
				if (int.TryParse(text, out year)) return year;
			}
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			return null;
		}

		private Dictionary<string, string> GetProperties(IHtmlDocument document) {
			var query = "#subcatfields table tr";
			var rows = document.QuerySelectorAll<IHtmlTableRowElement>(query);
			return rows.ToDictionary(row => row.Cells[0].TextContent.Trim().TrimEnd(':'), row => row.Cells[1].TextContent.Trim());
		}

		private string GetPropertyValue(Dictionary<string, string> properties, string key, Func<string, string> normalize) {
			if (properties.ContainsKey(key)) {
				if (normalize != null) return normalize(properties[key]);
				return properties[key];
			}
			return null;
		}
	}
}
