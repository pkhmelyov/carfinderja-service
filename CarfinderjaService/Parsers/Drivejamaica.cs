﻿using System.Collections.Generic;
using System.Linq;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class Drivejamaica : Parser {
	    public Drivejamaica(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.DrivejamaicaCom; } }
		public override string FirstPageAddress { get { return "http://www.drivejamaica.com/"; } }
		public override Url BaseUrl { get { return new Url(FirstPageAddress); } }

		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			return new[] {FirstPageAddress};
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			var query = "#classifiedScrollers > .categoryColumn";
			var columns = document.QuerySelectorAll<IHtmlDivElement>(query);
			if (columns == null) return null;
			var hrefs =
				columns.Take(5).SelectMany(col => col.QuerySelectorAll<IHtmlAnchorElement>("ul > li > a")).Select(a => a.Href);
			return hrefs;
		}

        private string GetFullName(IHtmlDocument document)
        {
            var query = "#advert_content > div.span5 > p";
            var p = document.QuerySelector<IHtmlParagraphElement>(query);
            if (p == null)
            {
                query = "#advert_content > div.span12 > p";
                p = document.QuerySelector<IHtmlParagraphElement>(query);
            }
            if (p == null) return null;
            var text = p.TextContent.Trim();
            return text;
        }

        private string TrimPhone(string text)
        {
            var index = text.IndexOf("TELE:");
            if (index >= 0) text = text.Substring(0, index).Trim();
            return text;
        }

        private string TrimPrice(string text)
        {
            var index = text.IndexOf("$");
            if (index >= 0) text = text.Substring(0, index).Trim();
            return text;
        }

		protected override string GetName(IHtmlDocument document) {
            var text = GetFullName(document);
            if (text == null) return null;
            int index = text.IndexOf(')');
            if (index >= 0) text = text.Substring(index + 1).Trim();
            text = TrimPhone(text);
            text = TrimPrice(text);
			return text;
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var query = "#advert_content > div.span6 > div.slideshow > img";
			var img = document.QuerySelector<IHtmlImageElement>(query);
			if(img == null) return DefaultThumbnailUrl;
			if(string.IsNullOrEmpty(img.Source)) return DefaultThumbnailUrl;
			return img.Source;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
            var text = GetFullName(document);
            if (text == null) return null;
            text = TrimPhone(text);
            var index = text.IndexOf('$');
            if (index < 0) return null;
            text = text.Substring(index + 1).Trim().TrimEnd('.');
            var multiplier = 1;
            if (text.IndexOf("Mil") > 0) multiplier = 1000000;
            else if (text.IndexOf("K") > 0) multiplier = 1000;
            text = text.Replace("Mil", "").Replace("K", "").Replace('.', ',').Trim();
            decimal result;
            if (decimal.TryParse(text, out result)) return result * multiplier;
            return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			return null;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
            var text = GetFullName(document);
            if (text == null) return null;
            var index = text.IndexOf("TELE:");
            if (index >= 0) return text.Substring(index + 5).Trim();
            return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			return null;
		}
	}
}