﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class Autoforestjamaica : Parser {
	    public Autoforestjamaica(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.AutoforestjamaicaCom; } }
		public override string FirstPageAddress { get { return "http://autoforestjamaica.com/car-listing/?per_page=60"; } }
		public override Url BaseUrl { get { return new Url("http://autoforestjamaica.com/"); } }
		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			string query = "a.page-numbers:not(.next)";
			var anchor = document.QuerySelectorAll<IHtmlAnchorElement>(query).LastOrDefault();
			if (anchor == null) {
				return new string[] {FirstPageAddress};
			}
			int page = int.Parse(new Url(anchor.Href).Path.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries).Last());
			return
				Enumerable.Range(1, page).Select(n => string.Format("http://autoforestjamaica.com/car-listing/{0}/?per_page=60", n));
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			var query = "article.car-entry div.detailed a.button";
			int i = 1;
			return document.QuerySelectorAll<IHtmlAnchorElement>(query).Select(x => string.Format("{0}?uc={1}", x.Href, i++));
		}

		protected override string GetName(IHtmlDocument document) {
			var query = "h2.section-title";
			var heading = document.QuerySelector<IHtmlHeadingElement>(query);
			return heading.TextContent;
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var query = "#car_slider li";
			var lis = document.QuerySelectorAll<IHtmlListItemElement>(query);
			var li = lis.FirstOrDefault();
			if (li == null) return DefaultThumbnailUrl;
			var img = li.QuerySelector<IHtmlImageElement>("img");
			if (img == null || img.Source.Contains("images/no-image")) return DefaultThumbnailUrl;
			return img.Source;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			var query = "div.price span";
			var span = document.QuerySelector<IHtmlSpanElement>(query);
			if (span != null) {
				var strPrice = span.GetAttribute("data-convert");
				decimal result;
				if (decimal.TryParse(strPrice, out result)) {
					return result;
				}
			}
			return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			var properties = GetCarProperties(document);
			var key = "Gearbox";
			if (properties.ContainsKey(key)) {
				Transmission result;
				if (Enum.TryParse(properties[key], true, out result)) {
					return result;
				}
			}
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			var properties = GetCarProperties(document);
			var key = "YOR";
			if (properties.ContainsKey(key)) {
				int result;
				if (int.TryParse(properties[key], out result)) {
					return result;
				}
			}
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			var properties = GetContactDetails(document);
			var key = "Contact Person";
			if (properties.ContainsKey(key)) {
				return properties[key];
			}
			return null;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			var properties = GetContactDetails(document);
			var phoneKey = "Phone";
			var mobileKey = "Mobile";
			var phones = new List<string>();
			if(properties.ContainsKey(phoneKey)) phones.Add(properties[phoneKey]);
			if(properties.ContainsKey(mobileKey)) phones.Add(properties[mobileKey]);
			if (phones.Count > 0) return string.Join(", ", phones.Distinct());
			return null;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			var properties = GetContactDetails(document);
			var key = "Address";
			if (properties.ContainsKey(key)) return properties[key];
			return null;
		}

		private Dictionary<string, string> GetAllProperties(IHtmlDocument document, string query) {
			var lis = document.QuerySelectorAll<IHtmlListItemElement>(query);
			return lis.ToDictionary(li => li.QuerySelector("b").TextContent.Replace(":", "").Trim(), li => li.QuerySelector("span").TextContent.Trim());
		}

		private Dictionary<string, string> GetCarProperties(IHtmlDocument document) {
			var query = "ul.type-car-position li";
			return GetAllProperties(document, query);
		}

		private Dictionary<string, string> GetContactDetails(IHtmlDocument document) {
			var query = "ul.contact-items li";
			return GetAllProperties(document, query);
		}
	}
}