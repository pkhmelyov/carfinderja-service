﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Parser.Html;
using CarfinderjaService.Services;
using CarfinderjaWeb.Data;
using CarfinderjaWeb.Data.Entities;

namespace CarfinderjaService.Parsers
{
    public abstract class Parser
    {
        protected readonly string DefaultThumbnailUrl = string.Format("http://placehold.it/700x400?text={0}", HttpUtility.UrlEncode("No photo"));

        private readonly AdvertisementsService _advertisementsService;
        private readonly Downloader _downloader;

        public abstract WebSite WebSite { get; }
        public abstract string FirstPageAddress { get; }
        public abstract Url BaseUrl { get; }

        public Url FirstPageUrl { get { return new Url(FirstPageAddress); } }

        public Parser(AdvertisementsService advertisementsService, Downloader downloader)
        {
            _advertisementsService = advertisementsService;
            _downloader = downloader;
        }

        #region Core

        public IHtmlDocument ParseHtml(string html)
        {
            if (html == null) return null;
            var parser = new HtmlParser();
            return parser.Parse(html);
        }

        #endregion

        #region Base Parsing Logic

        public void Parse()
        {
            _downloader.Download(FirstPageAddress, ProcessFirstPage);
        }

        public void ProcessFirstPage(string html)
        {
            var document = ParseHtml(html);
            if (document == null) return;
            var makes = GetMakes(document);
            if (makes != null)
            {
                _advertisementsService.AddMakesAndModels(makes);
            }
            IEnumerable<string> pagingUrls = GetPagingUrls(document);
            var pagesTasks = new List<Task<IHtmlDocument>> { Task.FromResult(document) };
            ProcessNthPage(html);
            foreach (var url in pagingUrls.Skip(1))
            {
                _downloader.Download(url, ProcessNthPage);
            }
        }

        public void ProcessNthPage(string html)
        {
            var document = ParseHtml(html);
            IEnumerable<string> adUrls = GetAdvertisementUrls(document);
            foreach (var url in adUrls)
            {
                string localUrl = url;
                _downloader.Download(url, content => ProcessAdPage(localUrl, content));
            }
        }

        public void ProcessAdPage(string url, string html)
        {
            var document = ParseHtml(html);
            if (document == null) return;
            Advertisement ad = ParseAdPage(document);
            if (ad.Title == null
                && ad.Price == null
                && ad.Transmission == null
                && ad.Year == null
                && ad.ConatactName == null
                && ad.ContactPhone == null
                && ad.ContactEmail == null
                && ad.Location == null) return;
            ad.Url = url;
            ad.WebSite = WebSite;
            ad.AddedOnUtc = DateTime.UtcNow;
            _advertisementsService.RecognizeMakeAndModel(ad);
            SaveAd(ad);
        }

        public virtual Advertisement ParseAdPage(IHtmlDocument document)
        {
            var ad = new Advertisement();
            ParseAdvertisementPage(ad, document);
            return ad;
        }

        public void SaveAd(Advertisement ad)
        {
            _advertisementsService.AddOrUpdate(ad);
        }

        #endregion

        protected abstract IEnumerable<string> GetPagingUrls(IHtmlDocument document);

        protected abstract IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document);

        private void ParseAdvertisementPage(Advertisement target, IHtmlDocument document)
        {
            target.Title = GetName(document);
            target.ThumbnailUrl = GetThumbnailUrl(document);
            target.Price = GetPrice(document);
            target.Transmission = GetTransmission(document);
            target.Year = GetYear(document);
            target.ConatactName = GetContactName(document);
            target.ContactPhone = GetContactPhone(document);
            target.ContactEmail = GetContactEmail(document);
            target.Location = GetLocation(document);
        }

        protected abstract string GetName(IHtmlDocument document);

        protected abstract string GetThumbnailUrl(IHtmlDocument document);

        protected abstract decimal? GetPrice(IHtmlDocument document);

        protected abstract Transmission? GetTransmission(IHtmlDocument document);

        protected abstract int? GetYear(IHtmlDocument document);

        protected abstract string GetContactName(IHtmlDocument document);

        protected abstract string GetContactPhone(IHtmlDocument document);

        protected abstract string GetContactEmail(IHtmlDocument document);

        protected abstract string GetLocation(IHtmlDocument document);

        public virtual Dictionary<string, List<string>> GetMakes(IHtmlDocument document)
        {
            return new Dictionary<string, List<string>>();
        }
    }
}