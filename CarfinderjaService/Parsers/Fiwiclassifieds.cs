﻿using System;
using System.Collections.Generic;
using System.Linq;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Extensions;
using Autofac;
using CarfinderjaWeb.Data;
using CarfinderjaService.Services;

namespace CarfinderjaService.Parsers {
	public class Fiwiclassifieds : Parser {
		private const string PricePropertyName = "Price";
		private const string TransmissionPropertyName = "Transmission";
		private const string YearPropertyName = "Year";
		private const string ContactNamePropertyName = "Name";
		private const string PhonePropertyName = "Cell Phone";

	    public Fiwiclassifieds(AdvertisementsService advertisementsService, Downloader downloader) : base(advertisementsService, downloader) { }

		public override WebSite WebSite { get { return WebSite.FiwiclassifiedsCom; } }
		public override string FirstPageAddress { get { return "https://jamaica.fiwiclassifieds.com/auto/cars/for-sale/"; } }
		public override Url BaseUrl { get { return new Url("https://jamaica.fiwiclassifieds.com/"); } }
		protected override IEnumerable<string> GetPagingUrls(IHtmlDocument document) {
			return new[] {FirstPageAddress};
		}

		protected override IEnumerable<string> GetAdvertisementUrls(IHtmlDocument document) {
			var query = "a.title";
			var anchors = document.QuerySelectorAll<IHtmlAnchorElement>(query);
			return anchors.Select(anchor => anchor.Href);
		}

		private bool TryExtractYear(ref string title, out int year) {
			var parts = title.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
			if (int.TryParse(parts.First(), out year)) {
				title = string.Join(" ", parts.Skip(1));
				return true;
			}
			if (int.TryParse(parts.Last(), out year)) {
				title = string.Join(" ", parts.Take(parts.Length - 1));
				return true;
			}
			return false;
		}

		private string InternalGetName(IHtmlDocument document, bool trimYear) {
			var query = "#advert-page > div.container-fluid > div.row-fluid > div.right-column > div.content-box-white > h2";
			var h2 = document.QuerySelector<IHtmlHeadingElement>(query);
			if (h2 != null) {
				var text = h2.TextContent.Split(new[] { " in " }, StringSplitOptions.RemoveEmptyEntries).First().Trim();
				if (trimYear) {
					int year;
					TryExtractYear(ref text, out year);
				}
				return text;
			}
			return null;
		}

		protected override string GetName(IHtmlDocument document) {
			return InternalGetName(document, trimYear: true);
		}

		protected override string GetThumbnailUrl(IHtmlDocument document) {
			var query = "#carousel img";
			var img = document.QuerySelector<IHtmlImageElement>(query);
			if (img != null) {
				var src = img.Source;
				if(string.IsNullOrEmpty(src)) return DefaultThumbnailUrl;
				return img.Source;
			}
			return DefaultThumbnailUrl;
		}

		protected override decimal? GetPrice(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var text = GetPropertyValue(properties, PricePropertyName,
				value => value.Replace("$", "").Replace(",", "").Replace(".", ",").Replace("JMD", "").Replace("(Negotiable)", "").Trim());
			if (text != null) {
				decimal price;
				if (decimal.TryParse(text, out price)) return price;
			}
			return null;
		}

		protected override Transmission? GetTransmission(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var text = GetPropertyValue(properties, TransmissionPropertyName);
			if (text != null) {
				Transmission result;
				if (Enum.TryParse(text, true, out result)) return result;
			}
			return null;
		}

		protected override int? GetYear(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var text = GetPropertyValue(properties, YearPropertyName);
			if (text != null) {
				int year;
				if (int.TryParse(text, out year)) return year;
			} else {
				var title = InternalGetName(document, trimYear: false);
				int year;
				if (TryExtractYear(ref title, out year)) return year;
			}
			return null;
		}

		protected override string GetContactName(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var text = GetPropertyValue(properties, ContactNamePropertyName);
			return text;
		}

		protected override string GetContactPhone(IHtmlDocument document) {
			var properties = GetAllProperties(document);
			var text = GetPropertyValue(properties, PhonePropertyName);
			return text;
		}

		protected override string GetContactEmail(IHtmlDocument document) {
			return null;
		}

		protected override string GetLocation(IHtmlDocument document) {
			var query = "#advert-page > div.container-fluid > div.row-fluid > div.right-column > div.content-box-white > h2";
			var h2 = document.QuerySelector<IHtmlHeadingElement>(query);
			if (h2 != null) {
				var text = h2.TextContent.Split(new[] {" in "}, StringSplitOptions.RemoveEmptyEntries).Last().Trim();
				return text;
			}
			return null;
		}

		private Dictionary<string, string> GetAllProperties(IHtmlDocument document) {
			var result = new Dictionary<string, string>();

			var query =
				"#advert-page > div.container-fluid > div.row-fluid > div.right-column > div.content-box-white > div.row-fluid > div.span6:nth-child(2) > table.info-box-shade tr";
			var rows = document.QuerySelectorAll<IHtmlTableRowElement>(query);
			if (rows != null) {
				result = rows.Select(row => row.Cells.Select(cell => cell.TextContent.Trim()).ToArray())
					.ToDictionary(ar => ar[0], ar => ar[1]);
			}

			query =
				"#advert-page > div.container-fluid > div.row-fluid > div.right-column > div.content-box-white > div.row-fluid > div.span6:nth-child(2) > div.content-box-grey > table tr";
			rows = document.QuerySelectorAll<IHtmlTableRowElement>(query);
			if (rows != null) {
				result = result.Union(rows.Select(row => row.Cells.Select(cell => cell.TextContent.Trim()).ToArray())
					.ToDictionary(ar => ar[0], ar => ar[1])).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
			}

			query =
				"#advert-page > .container-fluid > .row-fluid > .right-column > .row-fluid > .span12 > .content-box-white:first-child > .row-fluid > .pull-left > .content-box-grey > .row-fluid .row-fluid";
			var divs = document.QuerySelectorAll<IHtmlDivElement>(query);
			return result.Union(divs.Select(div => div.QuerySelectorAll<IHtmlDivElement>("div").Select(inner => inner.TextContent.Trim()).ToArray())
				.ToDictionary(vals => vals[0], vals => vals[1])).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
		}

		private string GetPropertyValue(Dictionary<string, string> properties, string key, Func<string, string> normalize = null) {
			if (!properties.ContainsKey(key)) return null;
			if (normalize != null) return normalize(properties[key]);
			return properties[key];
		}
	}
}