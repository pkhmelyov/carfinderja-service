﻿using System;
using System.Threading.Tasks;

namespace CarfinderjaService
{
    public struct DownloadTask
    {
        public DownloadTask(string url, Action<string> continuation)
        {
            Url = url;
            Status = DownloadTaskStatus.New;
            Continuation = continuation;
        }

        public string Url { get; private set; }
        public DownloadTaskStatus Status { get; set; }
        public Action<string> Continuation { get; private set; }
    }

    public enum DownloadTaskStatus
    {
        New,
        InProgress,
        Finished,
        Failed
    }
}