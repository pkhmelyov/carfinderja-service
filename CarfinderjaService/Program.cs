﻿using System;
using Autofac;
using CarfinderjaService.Parsers;
using CarfinderjaWeb.Data;
using System.Collections.Generic;
using System.Threading;
using CarfinderjaWeb.Services;
using CarfinderjaWeb.Data.Repositories;
using CarfinderjaService.Services;

namespace CarfinderjaService {
    class Program {
        private static IContainer Container { get; set; }

        private static void InitDI()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<ComponentsRegistrationModule>();
            builder.RegisterType<StatisticsService>().As<IStatisticsService>().InstancePerDependency();
            builder.Register(context => new Downloader(1000, 2000, 5, context.Resolve<IStatisticsService>(), context.Resolve<ISiteStatisticsRepository>())).As<Downloader>().SingleInstance();
            builder.RegisterType<AdvertisementsService>().AsSelf().InstancePerDependency();
            Container = builder.Build();
        }

        static int Main()
        {
            InitDI();
            using (var scope = Container.BeginLifetimeScope())
            {
                var parsers = new List<Parser>
                {
                    new Jacars(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Autoadsja(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Crystalvehiclesales(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new CarlandJamaica(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Jetconcars(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Claja(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Superlativeauto(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Wheelsandwheels(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Jamaicanclassifieds(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Jamaicaobserver(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Fiwiclassifieds(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Drivejamaica(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new Autoforestjamaica(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new CarsInJamaica(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new JamaicaAutoTrader(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                    new JamDeal(scope.Resolve<AdvertisementsService>(), scope.Resolve<Downloader>()),
                };

                scope.Resolve<AdvertisementsService>().RemoveMakeDuplicates();
                scope.Resolve<AdvertisementsService>().RecognizeMakesAndModelsForAllUnprocessedAds();
                var downloader = scope.Resolve<Downloader>();
                downloader.ComputeStats();

                foreach (Parser p in parsers) p.Parse();
                while (!downloader.Finished)
                {
                    Console.WriteLine(downloader.GetDownloadingStats());
                    Thread.Sleep(TimeSpan.FromMinutes(1));
                }
            }

            return 1;
        }
    }
}
