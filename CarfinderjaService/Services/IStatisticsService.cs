﻿using CarfinderjaWeb.Data;

namespace CarfinderjaWeb.Services
{
    public interface IStatisticsService
    {
        Statistics GetStatistics();
    }
}