﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarfinderjaWeb.Data;
using CarfinderjaWeb.Data.Entities;
using CarfinderjaWeb.Data.Repositories;

namespace CarfinderjaWeb.Services
{
    public class StatisticsService : IStatisticsService
    {
        private readonly IAdvertisementRepository _advertisementRepository;
        private readonly IMakeRepository _makeRepository;
        private Dictionary<int, Make> _allMakesDictionary;
        private Dictionary<int, Model> _allModelsDictionary;

        private Dictionary<int, Make> AllMakesDictionary { get { return _allMakesDictionary ?? (_allMakesDictionary = _makeRepository.GetMakes().ToDictionary(x => x.Id, x => x)); } }

        private Dictionary<int, Model> AllModelsDictionary { get { return _allModelsDictionary ?? (_allModelsDictionary = AllMakesDictionary.SelectMany(x => x.Value.Models).ToDictionary(x => x.Id, x => x)); } }

        public StatisticsService(IAdvertisementRepository advertisementRepository, IMakeRepository makeRepository)
        {
            _advertisementRepository = advertisementRepository;
            _makeRepository = makeRepository;
        }

        public Statistics GetStatistics()
        {
            var ads = _advertisementRepository.GetRecent().ToArray().Select(ad => new Tuple<Advertisement, PriceRange>(ad, GetPriceRange(ad))).ToArray();
            var ads1 = _advertisementRepository.GetRecent().Where(ad => ad.ModelId != null).ToArray();

            var result = new Statistics();
            result.CarsForSale = _advertisementRepository.GetRecent().Count();

            var today = DateTime.UtcNow.Date;
            var monthAgo = today.AddDays(-30);
            result.CarsAddedLastMonth = _advertisementRepository.GetRecent().Count(ad => ad.AddedOnUtc <= today && ad.AddedOnUtc >= monthAgo);

            result.Top10Models = ads1.Where(ad => AllModelsDictionary[ad.ModelId.GetValueOrDefault()].Name.Trim().ToUpperInvariant() != AllModelsDictionary[ad.ModelId.GetValueOrDefault()].Make.Name.Trim().ToUpperInvariant()).GroupBy(ad => AllModelsDictionary[ad.ModelId.GetValueOrDefault()])
                .OrderByDescending(adGroup => adGroup.Count())
                .Take(10)
                .Select(
                    adGroup =>
                        new AdvertisedModelStats
                        {
                            Model = string.Format("{0} {1}", adGroup.Key.Make.Name, adGroup.Key.Name),
                            TotalCount = adGroup.Count(),
                            PercentRate = (double)100 * adGroup.Count() / result.CarsForSale
                        })
                .ToList();

            result.Top10Brands =
                ads1.GroupBy(ad => AllMakesDictionary[ad.MakeId.GetValueOrDefault()])
                    .OrderByDescending(adGroup => adGroup.Count())
                    .Take(10)
                    .Select(adGroup => new AdvertisedBrandStats
                    {
                        Brand = adGroup.Key.Name,
                        TotalCount = adGroup.Count(),
                        PercentRate = (double)100 * adGroup.Count() / result.CarsForSale
                    }).ToList();

            var topBrand = result.Top10Brands.FirstOrDefault();
            if (topBrand != null)
            {
                topBrand.RelativePercentRate = 100;
                foreach (var item in result.Top10Brands.Skip(1))
                {
                    item.RelativePercentRate = (double) 100*item.TotalCount/topBrand.TotalCount;
                }
            }

            result.PriceRangeOverview =
                ads.GroupBy(ad => ad.Item2)
                    .OrderBy(adGroup => adGroup.Key)
                    .ToDictionary(adGroup => adGroup.Key, adGroup => new PercentRate { Base = (double)100 * adGroup.Count() / result.CarsForSale });
            if (result.PriceRangeOverview.Any()) {
                var maxPercent = result.PriceRangeOverview.Max(kvp => kvp.Value.Base);

                foreach (var item in result.PriceRangeOverview.Values)
                {
                    item.Relative = 100 * item.Base / maxPercent;
                }
            }

            return result;
        }

        private PriceRange GetPriceRange(Advertisement ad)
        {
            var price = ad.Price.GetValueOrDefault();
            if (price <= 500000) return PriceRange.I_0_500000;
            if (price > 500000 && price <= 1000000) return PriceRange.I_500000_1000000;
            if (price > 1000000 && price <= 1500000) return PriceRange.I_1000000_1500000;
            if (price > 1500000 && price <= 2000000) return PriceRange.I_1500000_2000000;
            if (price > 2000000 && price <= 3000000) return PriceRange.I_2000000_3000000;
            if (price > 3000000 && price <= 4000000) return PriceRange.I_3000000_4000000;
            if (price > 4000000 && price <= 5000000) return PriceRange.I_4000000_5000000;
            return PriceRange.I_GT_5000000;
        }
    }
}