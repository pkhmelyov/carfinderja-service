﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarfinderjaWeb.Data.Repositories;
using CarfinderjaWeb.Data;
using CarfinderjaWeb.Data.Entities;

namespace CarfinderjaService.Services {
	public class AdvertisementsService {
		private readonly IAdvertisementRepository _adverticementRepository;
		private readonly IMakeRepository _makeRepository;
        private IEnumerable<Make> _allMakes;
        private Dictionary<int, Make> _allMakesDictionary;

        private IEnumerable<Make> AllMakes { get { return _allMakes ?? (_allMakes = _makeRepository.GetMakes()); } }
        private Dictionary<int, Make> AllMakesDictionary { get { return _allMakesDictionary ?? (_allMakesDictionary = AllMakes.ToDictionary(x => x.Id, x => x)); } }

        public AdvertisementsService(IAdvertisementRepository adverticementRepository, IMakeRepository makeRepository) {
			_adverticementRepository = adverticementRepository;
			_makeRepository = makeRepository;
		}

		public Task AccountChanges(Advertisement[] ads) {
			foreach (var ad in ads) {
				AddOrUpdate(ad, saveChanges: false);
			}
			return _adverticementRepository.SaveChangesAsync();
		}

		public async Task ResetFlagAsync(WebSite site) {
			await _adverticementRepository.ResetFlagAsync(site);
			await _adverticementRepository.SaveChangesAsync();
		}

		public async Task DeleteNotUpdatedAsync(WebSite site) {
			await _adverticementRepository.DeleteUnchangedAsync(site);
			await _adverticementRepository.SaveChangesAsync();
		}

		private void UpdateAdvertisement(Advertisement source, Advertisement destination) {
			destination.Url = source.Url;
			destination.ThumbnailUrl = source.ThumbnailUrl;
			destination.Title = source.Title;
			destination.Price = source.Price;
			destination.Transmission = source.Transmission;
			destination.Year = source.Year;
			destination.ConatactName = source.ConatactName;
			destination.ContactPhone = source.ContactPhone;
			destination.ContactEmail = source.ContactEmail;
			destination.Location = source.Location;
		}

		public void AddOrUpdate(Advertisement ad, bool saveChanges = true) {
			var saved = _adverticementRepository.GetByUrl(ad.Url);
			if (saved == null) {
				_adverticementRepository.Add(ad);
				ad.Updated = true;
			} else {
				UpdateAdvertisement(ad, saved);
				saved.Updated = true;
			}
			if (saveChanges) _adverticementRepository.SaveChangesAsync().Wait();
		}

		#region Makes & Models

		public void AddMakesAndModels(Dictionary<string, List<string>> data) {
			AddMakes(data.Keys);
			foreach (var item in data) {
				AddModels(item.Key, item.Value);
			}
            _allMakes = null;
		}

		private void AddMakes(IEnumerable<string> makes) {
			foreach (var make in makes) {
				AddMake(make);
			}
			_makeRepository.SaveChanges();
			_allMakes = null;
		}

		private void AddMake(string make) {
			if(!MakeExists(make)) _makeRepository.Add(make, save: false);
		}

		private bool MakeExists(string make) {
			make = make.ToUpperInvariant();
			return
				AllMakes.Any(
					item => item.Name.ToUpperInvariant() == make || item.Aliases.Any(alias => alias.Alias.ToUpperInvariant() == make));
		}

		private void AddModels(string make, IEnumerable<string> models) {
			foreach (var model in models) {
				AddModel(make, model);
			}
			_makeRepository.SaveChanges();
		}

		private void AddModel(string make, string model) {
			var makeEntity = GetMake(make);
			if (!ModelExists(makeEntity, model)) makeEntity.Models.Add(new CarfinderjaWeb.Data.Entities.Model { Name = model });
		}

		private Make GetMake(string make) {
			make = make.ToUpperInvariant();
			return
				AllMakes.SingleOrDefault(
					item => item.Name.ToUpperInvariant() == make || item.Aliases.Any(alias => alias.Alias.ToUpperInvariant() == make));
		}

		private bool ModelExists(Make make, string model) {
			model = model.ToUpperInvariant();
			return
				make.Models.Any(
					item => item.Name.ToUpperInvariant() == model || item.Aliases.Any(alias => alias.Alias.ToUpperInvariant() == model));
		}

        public void RemoveMakeDuplicates()
        {
            var makesCache = new Dictionary<string, Make>();
            var makesFromDatabase = _makeRepository.GetMakes().OrderBy(x => x.Id);
            foreach(var item in makesFromDatabase)
            {
                var makeName = item.Name.Trim().ToUpperInvariant();
                if (makesCache.ContainsKey(makeName))
                {
                    MoveMakeAliases(item, makesCache[makeName]);
                    MoveMakeModels(item, makesCache[makeName]);
                    MoveMakeAdvertisments(item, makesCache[makeName]);
                    _makeRepository.DeleteMake(item, save: false);
                }
                else
                {
                    makesCache[makeName] = item;
                }
            }
            _makeRepository.SaveChanges();

            makesFromDatabase = _makeRepository.GetMakes().OrderBy(x => x.Id);
            foreach(var item in makesFromDatabase)
            {
                RemoveModelDuplicates(item);
            }
            _makeRepository.SaveChanges();
        }

        private void MoveMakeAliases(Make from, Make to)
        {
            var aliasesFrom = from.Aliases.ToArray();
            foreach(var alias in aliasesFrom)
            {
                from.Aliases.Remove(alias);
                to.Aliases.Add(alias);
                alias.MakeId = to.Id;
                alias.Make = to;
            }
        }

        private void MoveMakeModels(Make from, Make to)
        {
            var modelsFrom = from.Models.ToArray();
            foreach(var model in modelsFrom)
            {
                from.Models.Remove(model);
                to.Models.Add(model);
                model.MakeId = to.Id;
                model.Make = to;
            }
        }

        private void MoveMakeAdvertisments(Make from, Make to)
        {
            var advertismentsFrom = _adverticementRepository.GetByMake(from.Id);
            foreach(var ad in advertismentsFrom)
            {
                ad.MakeId = to.Id;
            }
            _adverticementRepository.SaveChangesAsync().Wait();
        }

        private void RemoveModelDuplicates(Make make)
        {
            var modelsCache = new Dictionary<string, Model>();
            var modelsFromMake = make.Models.OrderBy(x => x.Id).ToArray();
            foreach(var item in modelsFromMake)
            {
                var modelName = item.Name.Trim().ToUpperInvariant();
                if (modelsCache.ContainsKey(modelName))
                {
                    MoveModelAliases(item, modelsCache[modelName]);
                    MoveModelAdvertisments(item, modelsCache[modelName]);
                    _makeRepository.DeleteModel(item, save: false);
                }
                else
                {
                    modelsCache[modelName] = item;
                }
            }
        }

        private void MoveModelAliases(Model from, Model to)
        {
            var aliasesFrom = from.Aliases.ToArray();
            foreach(var alias in aliasesFrom)
            {
                from.Aliases.Remove(alias);
                to.Aliases.Add(alias);
                alias.ModelId = to.Id;
                alias.Model = to;
            }
        }

        private void MoveModelAdvertisments(Model from, Model to)
        {
            var advertismentsFrom = _adverticementRepository.GetByModel(from.Id);
            foreach(var ad in advertismentsFrom)
            {
                ad.ModelId = to.Id;
            }
            _adverticementRepository.SaveChangesAsync().Wait();
        }

        public void RecognizeMakeAndModel(Advertisement ad)
        {
            RecognizeMake(ad);
            RecognizeModel(ad);
            ad.TriedToRecognizeMakeAndModel = true;
        }

        private void RecognizeMake(Advertisement ad)
        {
            if (string.IsNullOrWhiteSpace(ad.Title)) return;
            var title = ad.Title.Trim().ToUpperInvariant();
            foreach (var make in AllMakes)
            {
                var name = make.Name.Trim().ToUpperInvariant();
                if (title.Contains(name))
                {
                    ad.MakeId = make.Id;
                    break;
                }
                else
                {
                    var aliasMatched = false;
                    foreach (var alias in make.Aliases)
                    {
                        name = alias.Alias.Trim().ToUpperInvariant();
                        if (title.Contains(name))
                        {
                            ad.MakeId = make.Id;
                            aliasMatched = true;
                            break;
                        }
                    }
                    if (aliasMatched) break;
                }
            }
        }

        private void RecognizeModel(Advertisement ad)
        {
            if (ad.MakeId == null) return;
            var title = ad.Title.Trim().ToUpperInvariant();
            foreach (var model in AllMakesDictionary[ad.MakeId.GetValueOrDefault()].Models)
            {
                var name = model.Name.Trim().ToUpperInvariant();
                if (title.Contains(name))
                {
                    ad.ModelId = model.Id;
                    break;
                }
                else
                {
                    var aliasMatched = false;
                    foreach (var alias in model.Aliases)
                    {
                        name = alias.Alias.Trim().ToUpperInvariant();
                        if (title.Contains(name))
                        {
                            ad.ModelId = model.Id;
                            aliasMatched = true;
                            break;
                        }
                    }
                    if (aliasMatched) break;
                }
            }
        }

        public void RecognizeMakesAndModelsForAllUnprocessedAds()
        {
            while(true)
            {
                var ads = _adverticementRepository.GetUnprocessedAdsWithoutMakesAndModels(100);
                foreach (var ad in ads)
                {
                    RecognizeMakeAndModel(ad);
                }
                if (ads.Any()) _adverticementRepository.SaveChangesAsync().Wait();
                else break;
            }
        }

        #endregion
    }
}